const crypto = require('crypto'); //nodejs crypto utility library
const axios = require('axios'); // http request library
const querystring = require('querystring'); // nodejs querystring encoder/decoder library

//this function make md5 hashed version of the string(passwords)
function generateMD5(data) {
    return crypto.createHash('md5').update(data).digest("hex");
}


/************* FUNCTIONS *********************/
//function to get initial token.
async function getInitialToken(params) {
    let url = 'https://oauth.medocity.net/login?';
    let username = 'acme@medocity.com'
    let password = 'acme_password1'
    let response = await axios({
        method: 'post',
        url,
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
            //'Authorization': `Basic ${base64data}`,
        },
        auth: {
            username: username,
            password: generateMD5(password)
        }
    })
    // console.log("Data : ", response.data)
    return response.data;
}

//function to get an authorizationCode
async function authorize(token) {
    let url = 'https://oauth.medocity.net/authorize';
    let response = await axios.post(url,
        querystring.stringify({
            client_id: 'acme',
            client_secret: 'acme_secret',
            state: 'my state',
            response_type: "code"
        }), {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": `Bearer ${token}`,
        }
    })

    return response.data;
}

//function to exchange authorizationCode with an access_code
async function getAccessToken(authorizationCode) {
    let url = 'https://oauth.medocity.net/token';
    let response = await axios.post(url,
        querystring.stringify({
            client_id: 'acme',
            client_secret: 'acme_secret',
            grant_type: 'authorization_code',
            code: authorizationCode
        }), {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    })

    return response.data;
}
/************* FUNCTIONS ENDS *********************/


/************* FLOW START HERE *********************/
//start initial login
getInitialToken()
    .then(tokenData => {
        console.log("initial login : ", tokenData);
        return authorize(tokenData.message.token)// we've got the token now call authorize() with it
    })
    .then(authCodeData => {
        console.log('authCodeData : ', authCodeData);
        return getAccessToken(authCodeData.authorizationCode)// authcode received. now exchange it with an access_token
    })
    .then(data => {
        console.log('access_token:', data.accessToken)
        //sample request to medocity api enpoint with access token
        let apiUrl ='https://api.medocity.net/smarthome/vitals';
        axios.get(apiUrl, {
            headers:{
                'Ocp-Apim-Subscription-Key':'xxxxxxx', // you can get this key from our api portal under your profile section.
                "Authorization": `Bearer ${data.accessToken}`
            }
        })
        .then(response => console.log(response.data))
    })
    .catch(err => console.log(err.message))
    
    
/************* FLOW END HERE *********************/